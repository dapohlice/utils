package code.pohli.utils.console;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Console {
	
	private final String WARNING = "WARNING";
	private final String LOG = "LOG";
	private final String ERROR = "ERROR";
	private final String DEBUG = "DEBUG";
	
	private boolean verbose = true;
	private boolean log = false;
	private boolean debug = false;
	
	private Path logFile;
	private Path logWarningFile;
	private Path logErrorFile;
	
	private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ssZ");
	
	
	
	public Console(
			boolean verbose, 
			boolean log, 
			Path logFile, 
			Path logErrorFile, 
			Path logWarningFile
			) {
		super();
		
		if(logFile == null || logErrorFile == null || logWarningFile == null)
		{
			throw new NullPointerException("Console: Log Paths");
		}
		
		this.verbose = verbose;
		this.log = log;
		this.logFile = logFile;
		this.logWarningFile = logWarningFile;
		this.logErrorFile = logErrorFile;
	}
	
	public Console(
			boolean verbose, 
			boolean log, 
			String logFile, 
			String logErrorFile, 
			String logWarningFile
			) {
		
		this(
				verbose,
				log,
				Paths.get(logFile),
				Paths.get(logWarningFile),
				Paths.get(logErrorFile));
	}
	
	public Console(boolean verbose, boolean log, Path logFile,Path logErrorFile) {
		this(
				verbose,
				log,
				logFile,
				logErrorFile,
				logFile
				);
	}
	
	public Console(
			boolean verbose, 
			boolean log, 
			String logFile, 
			String logErrorFile
			) {
		
		this(
				verbose,
				log,
				Paths.get(logFile),
				Paths.get(logErrorFile));
	}
	
	public Console(boolean verbose, boolean log, Path logFile) {
		this(
				verbose,
				log,
				logFile,
				logFile,
				logFile
				);
	}
	
	public Console(
			boolean verbose, 
			boolean log, 
			String logFile
			) {
		
		this(
				verbose,
				log,
				Paths.get(logFile)
			);
	}
	
	public Console(boolean verbose) {
		super();
		this.verbose = verbose;
		this.log = false;
		this.logFile = null;
		this.logWarningFile = null;
		this.logErrorFile = null;
	}
	
	
	public void setDebug(boolean debug)
	{
		this.debug = debug;
	}
	

	public void setVerbose(boolean verbose)
	{
		this.verbose = verbose;
	}

	public void setLog(Path logfile)
	{
		
		
		if(Files.notExists(logFile)) {
			try {
				Files.createFile(logfile);
			} catch (IOException e) {
				Console.error("Failed to create LogFile: "+logFile,e);
				return;
			}
			this.log = true;
			this.setAllLogFiles(logfile);
		}
		
		
	}
	
	private void setAllLogFiles(Path logfile)
	{
		this.logFile = logfile;
		this.logErrorFile = logfile;
		this.logWarningFile = logfile;
	}
	
	
	public void printWarn(String message)
	{
		String  text = String.format(
				"%-7s|%s|%s", 
				this.WARNING,
				Console.getDateString(),
				message
				);
		
		if(this.verbose)
		{
			System.out.println(text);
		}
		
		if(this.log)
		{
			try {
				Files.write(
						this.logWarningFile, 
						text.getBytes(Charset.forName("UTF-8")), 
						StandardOpenOption.APPEND);
			} catch (IOException e) {
				System.err.println(String.format(
						"Failed write to %s Log (%s) text: %s",
						this.WARNING,
						this.logWarningFile,
						message));
				e.printStackTrace();
			}
		}
	}
	
	public void printDebug(String message)
	{
		String  text = String.format(
				"%-7s|%s|%s", 
				this.DEBUG,
				Console.getDateString(),
				message
				);
		
		if(this.verbose && this.debug)
		{
			System.out.println(text);
		}
		
		if(this.log && this.debug)
		{
			try {
				Files.write(
						this.logFile, 
						text.getBytes(Charset.forName("UTF-8")), 
						StandardOpenOption.APPEND);
			} catch (IOException e) {
				System.err.println(String.format(
						"Failed write to %s Log (%s) text: %s",
						this.LOG,
						this.logFile,
						message));
				e.printStackTrace();
			}
		}
	}
	
	public void printLog(String message)
	{
		String  text = String.format(
				"%-7s|%s|%s", 
				this.LOG,
				Console.getDateString(),
				message
				);
		
		if(this.verbose)
		{
			System.out.println(text);
		}
		
		if(this.log)
		{
			try {
				Files.write(
						this.logFile, 
						text.getBytes(Charset.forName("UTF-8")), 
						StandardOpenOption.APPEND);
			} catch (IOException e) {
				System.err.println(String.format(
						"Failed write to %s Log (%s) text: %s",
						this.LOG,
						this.logFile,
						message));
				e.printStackTrace();
			}
		}
	}
	
	public void printError(String message)
	{
		String  text = String.format(
				"%-7s|%s|%s", 
				this.ERROR,
				Console.getDateString(),
				message
				);
		
		if(this.verbose)
		{
			System.err.println(text);
		}
		
		if(this.log)
		{
			try {
				Files.write(
						this.logErrorFile, 
						text.getBytes(Charset.forName("UTF-8")), 
						StandardOpenOption.APPEND);
			} catch (IOException e) {
				System.err.println(String.format(
						"Failed write to %s Log (%s) text: %s",
						this.ERROR,
						this.logErrorFile,
						message));
				e.printStackTrace();
			}
		}
	}
	
	public void printError(String message,Exception ex)
	{
		if(ex == null)
		{
			printError(message);
		}
		String cause="";
		if(ex.getCause() != null)
		{
			cause = ex.getCause().getMessage();
		}
		String text = String.format("%s%n%s%n%s", 
				message,
				ex.getMessage()
				,cause
				);
		
		this.printError(text);
	}
	
	
	public static Console console = new Console(true);
	
	public static void warn(String message)
	{
		Console.console.printWarn(message);
	}
	
	public static void log(String message)
	{
		Console.console.printLog(message);
	}

	public static void debug(String message)
	{
		Console.console.printDebug(message);
	}

	
	public static void error(String message)
	{
		Console.console.printError(message);
	}

	public static void error(String message,Exception ex)
	{
		Console.console.printError(message,ex);
	}
	
	
	
	
	public static String getDateString() {
		 return simpleDateFormat.format(new Date());
	}
	
}
